import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CalculatorTest {
    private Calculator calculator;

    @Before
    public void init(){
        this.calculator=new Calculator();
    }
    @Test
    public void checkisEmptyString() throws NumberFormatException {
        assertEquals(0,this.calculator.add(""));
    }

    @Test
    public void checkOneNumStr() throws NumberFormatException {
        assertEquals(1,this.calculator.add("1"));
    }

    @Test
    public void checkTwoNumStr() throws NumberFormatException {
        assertEquals(5,this.calculator.add("2,3"));
    }

    @Test
    public void checkNumberwithNewLines() throws NumberFormatException {
        assertEquals(6,this.calculator.add("1\n2,3"));
    }

    @Test
    public void checkWithInvalidInput() throws NumberFormatException {
        //if Invalid data return zero
        assertEquals(0,this.calculator.add("1,\n,3"));
    }

    @Test
    public void checkwithDelimiterStr() throws NumberFormatException {
        assertEquals(3,this.calculator.add("//;\n1;2"));
        //check other delimiter supports
        assertEquals(3,this.calculator.add("//'\n'1'2"));
    }

    @Test(expected = NumberFormatException.class)
    public void checkwithOneNegativeNumber() throws NumberFormatException {
        try {
            this.calculator.add("-100");
        } catch (NumberFormatException e){
            assertTrue(e.getMessage().equalsIgnoreCase("negative not allowed::-100,"));
            throw e;
        }
    }

    @Test(expected = NumberFormatException.class)
    public void checkwithTwoNegativeNumber() throws NumberFormatException {
        try {
            this.calculator.add("-100,-200");
        } catch (NumberFormatException e){
            assertTrue(e.getMessage().equalsIgnoreCase("negative not allowed::-100,-200,"));
            throw e;
        }
    }

    @Test
    public void checkValueGreaterThan_3_Digit(){
        assertEquals(2,this.calculator.add("1000,2"));
    }

    @Test
    public void checkwithDelimiterOfAnyLength(){
        assertEquals(6,this.calculator.add("//***\n1***2***3"));
    }

    @Test
    public void checkwithMultipleDelimiter(){
        assertEquals(6,this.calculator.add("//*%\n1*2%3"));
    }

    @Test
    public void checkwithNonNumericValue(){
        assertEquals(0,this.calculator.add("12,ab"));
    }
}
