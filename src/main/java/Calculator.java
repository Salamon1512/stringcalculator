import java.util.Arrays;

public class Calculator {
    public int add(String numbers) throws NumberFormatException {
        int sum=0;
        if(numbers == null || numbers.isEmpty()){
            return sum;
        }
        checkIsNegativeNumber(numbers);
        numbers=replaceNewLine(numbers);
        //checking for not allowed value
        if(numbers.contains(",,")){
            return sum;
        }
        //split the delimiter or commas
        String[] numArr=splitDelimiterOrCommas(numbers);
        if(numArr.length==1)
            return numArr.length;
        else {
            return doAddition(numArr);
        }
    }

    private int doAddition(String[] numArr) {
        int sum=0;
        numArr=checkNumber_GreaterThan_3_Digits(numArr);
        for(String val:numArr){
            if(!val.isEmpty())
                try {
                    sum += Integer.parseInt(val);
                } catch (NumberFormatException e){
                    sum=0;
                    break;
                }
        }
        return sum;
    }

    private String[] checkNumber_GreaterThan_3_Digits(String[] numArr) {
        for (int i=0;i<numArr.length;i++)
            if(numArr[i].trim().length()>3)
                numArr[i]="0";
        return numArr;
    }

    private void checkIsNegativeNumber(String numbers) {
        if(numbers.contains("-")) {
            StringBuffer strmsg=new StringBuffer("negative not allowed::");
            Arrays.stream(numbers.split(",")).forEach(s->strmsg.append(s+","));
            throw new NumberFormatException( strmsg.toString());
        }
    }

    private String replaceNewLine(String val){
        return val.trim().replaceAll("[\\n]",",");
    }

    private String[] splitDelimiterOrCommas(String value){
        return value.split("[,:;*%^$#//\"\']");
    }

}
